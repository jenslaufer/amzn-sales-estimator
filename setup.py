from setuptools import setup, find_packages

setup(
    name='amznestimator',
    url='https://github.com/jenslaufer/amzn-sales-estimator.git',
    author='Jens Laufer',
    author_email='jenslaufer@gmail.com',
    packages=['amzn'],  # find_packages()
    install_requires=['requests', 'bottlenose',
                      'lxml',
                      'scikit-learn',
                      'pandas', 'numpy', 'get_smarties',
                      ],
    version='0.2.0',
    license='MIT',
    description='estimator for sales based on BSR and category',
    include_package_data=True
)